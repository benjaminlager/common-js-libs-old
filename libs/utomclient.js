var http = require('http');
var request = require('request');
var extend = require('node.extend');
var logger = require('./logger');

http.globalAgent.maxSockets = Infinity;

function UtomClient(config) {
    this.config = config;
}

//private methods
function _handleRequestResponse(data, response, callback) {
    var problem = _resolveProblem(response.statusCode, data);
    callback(problem, data, response);
}

//not sure if we need this anymore...
function _formatSearchResult(data) {
    var result = [];
    var hits = {};
    if (data.searchResult) {
        hits = data.searchResult.hits;
    }

    if (hits instanceof Array) {
        hits.forEach(function (item, i) {
            result.push(item);
        });
    } else {
        result.push(hits);
    }
    data.searchResult.hits = result;
    var res = {
        "result": data.searchResult
    };
    return res;
}

function _searchStream(url, query, accessKey, cb) {
    if (query.q) {

        var headers = {
            "Accept": "application/hal+json",
            "Authorization": accessKey
        };

        var qs = query;

        request.get({url: url, headers: headers, qs:qs }, function (err, response, data) {
            if (err) {
                logger.error('Error', err);
                cb(err);
            } else {
                var jsonData = JSON.parse(data);
                var results = _formatSearchResult(jsonData);
                results.userQuery = query.q;
                cb(null, results, response);
            }
        });
    }
}

function searchStream(query, accessKey, cb) {
    _searchStream(this.config.stream, query, accessKey, cb);
}

function searchNotifications(query, accessKey, cb) {
    _searchStream(this.config.notifications, query, accessKey, cb);
}

function extractToken(req) {
    var user = req.user;
    var token = null;
    if (user) {
        token = req.user.token;
    }
    return token;
}

function getItemToIndexLocation(location, headers, cb) {
    var self = this;
    var h = {"Accept": "application/json"};
    extend(true, headers, h);
    var url = self.config.host + location;
    logger.info("Get item to index url ", url);

    request.get({json: true, url: url, headers: headers }, function (err, response, data) {
        if (err) {
            logger.error('Error');
            cb(err);
        } else {
            _handleRequestResponse(data, response, function (err) {
                cb(err, data);
            });
        }
    });
}

function connectRelation(url, token, rel, cb) {
    var self = this;
    var headers = {
        "Content-Type": "text/uri-list",
        "Authorization": token
    };
    logger.debug("Connection relation to " + url);
    request.put({url: url, headers: headers, body:rel }, function (err, response, data) {
        if (err) {
            logger.error('Error', err);
            cb(err);
        } else {

            _handleRequestResponse(data, response, function (err) {
                if(cb){
                    cb(err, data);
                }
            });
        }
    });
}

function login(username, password, cb) {

    var form = {"username": username, "password": password};
    var headers = {"Content-Type": "application/x-www-form-urlencoded"};

    request.post({
        url: this.config.utomapi.login,
        form: form,
        headers: headers
    }, function onLoginResponse(error, response, data) {
        if (error) {
            cb(_resolveProblem(502, null, "Login is temporarily unavailable", "login-service-unavailable"), null);
        } else {
            _handleRequestResponse(data, response, cb);
        }
    });
}

function loginFull(username, password, cb) {
    var self = this;
    this.login(username, password, function onLogin (error, data, response) {
        if (error) {
            cb(error, null);
        } else {
            logger.debug("Login statuscode %s", response.statusCode);

            var setCookie = response.headers["set-cookie"][0];
            var at = setCookie.split("at=").pop();
            at = at.split(";").shift();

            self.getCurrentUser(at, function (error, user) {
                var loggedinuser = user;
                loggedinuser.token = at;
                cb(error, loggedinuser);
            });
        }
    });
}

function loginFacebook(facebookUser, cb) {
    var self = this;

    var payload = {
        url: this.config.utomapi.host + "/user/facebook/login",
        body: JSON.stringify(facebookUser),
        headers: {"Content-Type": "application/json"}
    };

    request.post(payload, function onLoginResponse(error, response, data) {
        if (error) {
            logger.error("FB login error", error);
            cb(_resolveProblem(502, null, payload.url + " is temporarily unavailable", "login-service-unavailable"), null);
        } else {
            _handleRequestResponse(data, response, function onFBLogin (problem, data, response) {
                if (problem) {
                    cb(problem, null);
                } else {
                    var setCookie = response.headers["set-cookie"][0];
                    var at = setCookie.split("at=").pop();
                    at = at.split(";").shift();

                    self.getCurrentUser(at, function (error, user) {
                        var loggedinuser = user;
                        loggedinuser.token = at;
                        cb(error, loggedinuser);
                    });
                }

            });
        }
    });

}

function saveResource(type, item, token, callback) {

    var data = item.data;
    var headers = {"Content-Type": "application/json"};
    var url = this.config.utomapi[type];

    var method = "post";
    if (item.data.id) {
        if(!token){
            token = this.config.utomapi.accessKey;
        }
        method = "patch";
        url += "/" + item.data.id;
    }

    if (token) {
        headers.Authorization = token;
    }


    logger.info("Saving : ", item.data, token, url);


    request[method]({
        url: url,
        json: true,
        body: data,
        headers: headers
    }, function onSaveResource(error, response, data) {
        if (error) {
            callback(error, null);
        } else {
            _handleRequestResponse(data, response, callback);
        }
    });
}

function deleteResource(type, item, token, callback) {
    var headers = {"Content-Type": "application/json"};

    if (token) {
        headers.Authorization = token;
    }
    var url = this.config.utomapi[type] + "/" + item.data.id;

    logger.info("Deleting : ", item.data);

    request({
        method: "delete",
        url: url,
        json: true,
        headers: headers
    }, function onDeleteResource(error, response, data) {
        if (error) {
            callback(error, null);
        } else {
            _handleRequestResponse(data, response, callback);
        }
    });
}

function unfollow(params, token, callback) {
    var self = this;
    var headers = {"Content-Type": "application/json"};

    if (token) {
        headers.Authorization = token;
    }
    var url = this.config.utomapi["find-follow-" + params.type];
    var args = {
        id: params.id,
        followerId: + params.userId
    };

    request.get({
        url: url,
        json: true,
        qs: args,
        headers: headers
    }, function onResponse(error, response, data) {
        if (error) {
            callback(error, null);
        } else {
            _handleRequestResponse(data, response, function onFound(error, data) {
                var item = {
                    data: {
                        id: data.itemId
                    }
                };
                self.deleteResource(params.action, item, token, function onDeleted(error, data, response) {
                    callback(error, null, response);
                });
            });
        }
    });
}


function getCurrentUser(at, cb) {

    var headers = {
        "Content-Type": "application/hal+json",
        "Authorization": at
    };

    var url = this.config.utomapi.user + "/current";

    request.get({json: true, url: url, headers: headers }, function onGetUser (err, response, data) {
        if (err) {
            logger.error('Error', err);
            cb(err, null);
        } else {
            _handleRequestResponse(data, response, function (err) {
                cb(err, data);
            });
        }
    });
}

function getModel(cb) {

    var url = this.config.utomapi.sports;

    request.get({json: true, url: url }, function onGetModel (err, response, data) {
        if (err) {
            logger.error('Error', err);
            cb(err, null);
        } else {
            cb(null, data, response);
        }
    });
}

function getMultimediaImageCoordinates(query, cb) {
    var url = this.config.searchbase +"/imageCoordinates";

    console.log(url, query);
    request.get(
        {json: true, url: url ,qs: query}, function onGetMultimedia (err, response, data) {
            if (err) {
                logger.error('Error', err);
                cb(err, null);
            } else {
                cb(null, data, response);
            }
        });
}



function pageIds(url, headers, type, cb) {

    request.get({json: true, url: url, headers: headers.headers}, function onPageIds(err, response, hal) {
        if (err) {
            logger.error('Error', err);
            cb(err, null);
        } else {
            console.log(response.statusCode); //error handling if not logged in

            if (hal._embedded) {
                var res = hal._embedded[type];
                if(!res){
                    res = hal._embedded["oo:"+type];
                }

                var data = [];
                res.forEach(function (val, i) {
                    var id = val._links.self.href.split("/").reverse()[0];
                    data.push({id: id});
                });
                cb(null, {data: data, next: hal._links.next});

            } else {
                cb(new Error("nothing _embeddded found on url ", url));
            }
        }
    });
}

function getItemToIndex(id, headers, type, cb) {
    if (!id) {
        cb(new TypeError("Id and type required"));
    } else {
        var self = this;
        var h = {"Accept": "application/json"};
        extend(true, headers.headers, h);

        var url = self.config.indexUrl + "/" + type + "?id=" + id;
        logger.info("Get item to index url ", url);
        request.get({json: true, url: url, headers: headers.headers}, function onGetItemToIndex(err, response, data) {
            if (err) {
                logger.error('Error', err);
                cb(err);
            } else {
                _handleRequestResponse(data, response, function (err) {
                    cb(err, data);
                });
            }
        });
    }

}

function get(url, args, auth, cb) {

    if(!auth){
        auth = this.config.utomapi.accessKey;
    }
    var h = {"Authorization": auth, "Accept":"application/hal+json"};
    extend(true, args.headers, h);

    request.get({url: url, headers: h, qs: args.qs}, function onGetItem(err, response, data) {
        if (err) {
            logger.error("Error in utomclient.GET", err);
            cb(err);
        } else {
            cb(null, JSON.parse(data), response);
        }

    });
}

function post(url, args, auth, cb) {

    var h = {"Authorization": auth};
    extend(true, args.headers, h);

    request.post({
        url: url,
        body: args.data,
        headers: args.headers
    }, function onLoginResponse(error, response, data) {
        if (error) {
            cb(error);
        } else {
            cb(null, data, response);
        }
    });
}

function _resolveProblem(statusCode, json, message, internalRef) {
    if (statusCode >= 300) {
        if (typeof json === "string") {
            json = JSON.parse(json);
        }
        logger.error("statusCode:" + statusCode + ", message:" + message + ", internalRef:" + internalRef + ", json:", json);
        return new UtomProblem(statusCode, json, message, internalRef);
    } else {
        return null;
    }
}

function UtomProblem(statusCode, json, message, internalRef) {
    this.type = resolveType();
    this.statusCode = statusCode;
    this.message = message ? message : resolveMessage(json);
    this.id = json ? json.id : null;
    this.ref = json ? json.internalRef : internalRef;
    this.violations = json.violations;

    function resolveType() {
        var codes = [302, 403, 401];
        if (codes.indexOf(statusCode) > -1) {
            return "loginrequired";
        }
        if (statusCode === 410) {
            return "unavailable";
        }
        if (statusCode >= 400 && statusCode < 500) {
            return "usererror";
        }
        if (statusCode >= 500) {
            return "systemerror";
        }


        return "unknown";
    }

    function resolveMessage() {
        if (json.cause) { // todo: Hack - SDR exposes "raw" exceptions! Don't want that!
            return "A system error occured. Please contact system admininstrator";
        } else if (json.detail) {
            return json.detail;
        } else {
            return "";
        }
    }
}

function searchActivities(query,token, cb) {
    if (query.q) {
        logger.info("Activity query", query);
        logger.info("Activity url", this.config.activity);

        this.get(this.config.activity, {qs: query},token, function (err, data, response) {
            if(err){
                cb(new Error(data));
            }else {
                cb(null, data, response);
            }
        });
    }
}

function search(query,token, cb) {
    if (query.q) {
        logger.info("Search query", query);
        logger.info("Search url", this.config.search);

        this.get(this.config.search, {qs: query},token, function (err, data, response) {
            if(err){

            }else {
                var results = {};
                if (response.statusCode >= 400) {
                    logger.error("Search status code: " + response.statusCode + ". Details: " + data);
                    cb(new Error(data));
                } else {



                    results = _formatSearchResult(data);
                    if (data._searchTime) {
                        logger.info("Time to seach " + data._searchTime);
                    }
                    cb(null, results, response);
                }
            }
        });
    }
}

function searchUser(query, cb) {
    if (query.q) {
        this.get(this.config.user, {qs: query },null, function (err, data, response) {

            var results = _formatSearchResult(data);
            results.userQuery = query.q;
            cb(null, results, response);
        });
    } else {
        cb(new Error("No query defined"));
    }

}

//new client
UtomClient.prototype.getItemToIndexLocation = getItemToIndexLocation;
UtomClient.prototype.saveResource = saveResource;
UtomClient.prototype.deleteResource = deleteResource;
UtomClient.prototype.unfollow = unfollow;
UtomClient.prototype.login = login;
UtomClient.prototype.loginFull = loginFull;
UtomClient.prototype.loginFacebook = loginFacebook;
UtomClient.prototype.getCurrentUser = getCurrentUser;
UtomClient.prototype.pageIds = pageIds;
UtomClient.prototype.getItemToIndex = getItemToIndex;
UtomClient.prototype.getModel = getModel;
UtomClient.prototype.get = get;
UtomClient.prototype.post = post;
UtomClient.prototype.extractToken = extractToken;
UtomClient.prototype.searchNotifications = searchNotifications;
UtomClient.prototype.searchStream = searchStream;
UtomClient.prototype.search = search;
UtomClient.prototype.searchUser = searchUser;
UtomClient.prototype.connectRelation = connectRelation;
UtomClient.prototype.searchActivities = searchActivities;
UtomClient.prototype.getMultimediaImageCoordinates = getMultimediaImageCoordinates;


module.exports = UtomClient;